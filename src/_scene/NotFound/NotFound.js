import React from 'react';
import Layout from "../../_component/Layout/Layout";

function NotFound(props) {
  return (
    <Layout>
      <h1>404 - Page non trouvée</h1>
    </Layout>
  );
}

export default NotFound;