import React from 'react';
import Layout from "../../_component/Layout/Layout";

function Home(props) {
  return (
    <Layout>
      <h1>Home</h1>
    </Layout>
  );
}

export default Home;