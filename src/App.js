import AppRoutes from "./_routes/routes";
import './_assets/styles/global.scss';


function App() {
  return (
    <div className="App">
      <AppRoutes/>
    </div>
  );
}

export default App;
