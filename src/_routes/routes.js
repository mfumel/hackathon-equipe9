import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from "../_scene/Login/Login";
import Home from "../_scene/Home/Home";
import ChoicePlanet from "../_scene/ChoicePlanet/ChoicePlanet";
import StartSituation from "../_scene/StartSituation/StartSituation";
import Game from "../_scene/Game/Game";
import Result from "../_scene/Result/Result";
import EnsureRoute from "./EnsureRoute";
import NotFound from "../_scene/NotFound/NotFound";

const AppRoutes = () => (
  <Router>
    <Routes>
      <Route path="/connexion" element={<Login/>} />
      <Route path="/" element={<EnsureRoute><Home/></EnsureRoute>} />
      <Route path="/situation-depart" element={<EnsureRoute><StartSituation/></EnsureRoute>} />
      <Route path="/jeu" element={<EnsureRoute><Game/></EnsureRoute>} />
      <Route path="/resultat" element={<EnsureRoute><Result/></EnsureRoute>} />
      <Route path="*" element={<EnsureRoute><NotFound/></EnsureRoute>} />
    </Routes>
  </Router>
);

export default AppRoutes;