import { Navigate, useLocation } from 'react-router-dom';

const EnsureLogin = ({ children }) => {
  // Remplace ceci avec ta propre logique de vérification de l'état de connexion
  const isLoggedIn = false;

  const location = useLocation();

  return isLoggedIn ? children : <Navigate to="/connexion" state={{ from: location }} replace />;
};

export default EnsureLogin;